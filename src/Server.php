<?php

namespace Vulpes\Http;

class Server
{
    protected InputInterface $input;

    public function __construct(InputInterface $input) { $this->input = $input; }

    public function isCommandLine(): bool { return php_sapi_name() === 'cli'; }

    public function isXmlHttpRequest(): bool
    {
        return $this->input->filter('HTTP_X_REQUESTED_WITH', FILTER_SANITIZE_STRING) === 'XMLHttpRequest';
    }

    public function getServerAddr()
    {
        return $this->input->filter('SERVER_ADDR', FILTER_DEFAULT);
    }

    public function getServerName()
    {
        return $this->input->filter('SERVER_NAME', FILTER_SANITIZE_STRING);
    }

    public function getServerProtocol()
    {
        return $this->input->filter('SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
    }

    public function getRequestMethod()
    {
        return $this->input->filter('REQUEST_METHOD', FILTER_DEFAULT);
    }

    public function getHttpConnection()
    {
        return $this->input->filter('HTTP_CONNECTION', FILTER_DEFAULT);
    }

    public function getHttpHost()
    {
        return $this->input->filter('HTTP_HOST', FILTER_SANITIZE_STRING);
    }

    public function getHttpReferer()
    {
        return $this->input->filter('HTTP_REFERER', FILTER_DEFAULT);
    }

    public function getHttpUserAgent()
    {
        return $this->input->filter('HTTP_USER_AGENT', FILTER_DEFAULT);
    }

    public function isHttps(): bool
    {
        return $this->input->filter('HTTPS', FILTER_SANITIZE_STRING) &&
          $this->input->filter('HTTPS', FILTER_SANITIZE_STRING) !== 'off';
    }

    public function getRemoteAddr()
    {
        return $this->input->filter('REMOTE_ADDR', FILTER_DEFAULT);
    }

    public function getRemoteHost()
    {
        return $this->input->filter('REMOTE_HOST', FILTER_DEFAULT);
    }

    public function getRemotePort()
    {
        return $this->input->filter('REMOTE_PORT', FILTER_DEFAULT);
    }

    public function getRemoteUser()
    {
        return $this->input->filter('REMOTE_USER', FILTER_DEFAULT);
    }

    public function getServerPort(): ?int
    {
        return $this->input->filter('SERVER_PORT', FILTER_SANITIZE_NUMBER_INT);
    }

    public function getRequestUri()
    {
        return $this->input->filter('REQUEST_URI', FILTER_SANITIZE_STRING);
    }
}