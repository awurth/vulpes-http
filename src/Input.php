<?php

namespace Vulpes\Http;

class Input implements InputInterface
{
    private int $type;

    public function __construct(int $type) { $this->type = $type; }

    public function filter(string $variableName, int $filter = FILTER_DEFAULT, array $options = null)
    {
        return filter_input($this->type, $variableName, $filter, $options);
    }

    public function filterArray(array $definition = null, $addEmpty = true)
    {
        return filter_input_array($this->type, $definition, $addEmpty);
    }
}