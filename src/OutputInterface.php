<?php

namespace Vulpes\Http;

interface OutputInterface
{
    public function load(): void;
}