<?php

namespace Vulpes\Http;

interface InputInterface
{
    public function filter(string $variableName, int $filter = FILTER_DEFAULT, array $options = null);

    public function filterArray(array $definition = null, $addEmpty = true);
}