<?php

namespace Vulpes\Http;

use Vulpes\Http\Exception\RuntimeException;

class Response
{
    protected OutputInterface $output;

    public function getOutput(): OutputInterface { return $this->output; }

    public function setOutput(OutputInterface $output): void { $this->output = $output; }

    public function redirect(string $location, int $httpResponseCode = 302): void
    {
        if (php_sapi_name() === 'cli') {
            exit('CLI redirect(' . $httpResponseCode . ')-location: ' . $location . PHP_EOL);
        }
        header('Location: ' . $location, true, $httpResponseCode);
        exit;
    }

    public function load(): void
    {
        if (is_null($this->output)) {
            throw new RuntimeException('There is no output interface initialized.');
        }
        $this->output->load();
    }
}