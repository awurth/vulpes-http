<?php

namespace Vulpes\Http;

class InputData implements InputInterface
{
    private array $storage;

    public function __construct(array $storage) { $this->storage = $storage; }

    public function filter(string $variableName, int $filter = FILTER_DEFAULT, array $options = null)
    {
        if (array_key_exists($variableName, $this->storage) === false) {
            return null;
        }
        return filter_var($this->storage[$variableName], $filter, $options);
    }

    public function filterArray(array $definition = null, $addEmpty = true)
    {
        return filter_var_array($this->storage, $definition, $addEmpty);
    }
}